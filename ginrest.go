package ginrest

import (
	"gitee.com/Ivy1996-encode/ginrest/ghttp/dispatch"
	"gitee.com/Ivy1996-encode/ginrest/grouter"
	"github.com/gin-gonic/gin"
)

// Shortcut for gin.Default
func Default() *gin.Engine {
	return gin.Default()
}

// Shortcut for dispatch.MakeStructAsHandlerFunc
func MakeStructAsHandlerFunc(view interface{}) gin.HandlerFunc {
	return dispatch.MakeStructAsHandlerFunc(view)
}

// Shortcut for grouter.NewRouter
func NewRouter(engine *gin.Engine, middleware ...gin.HandlerFunc) *grouter.Router {
	return grouter.NewRouter(engine, middleware...)
}

// Shortcut for grouter.NewDefaultRouter
func DefaultRouter(engine *gin.Engine, middleware ...gin.HandlerFunc) *grouter.DefaultRouter {
	return grouter.NewDefaultRouter(engine, middleware...)
}
