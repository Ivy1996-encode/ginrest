package gaction

import (
	"fmt"
	"gitee.com/Ivy1996-encode/ginrest/ghttp"
)

const (
	list          = "List"
	create        = "Create"
	retrieve      = "Retrieve"
	update        = "Update"
	partialUpdate = "PartialUpdate"
	destroy       = "Destroy"
	empty         = ""
)

var (
	GetAction           = NewAction(ghttp.MethodGet, empty, false, []string{ghttp.MethodGet})
	PostAction          = NewAction(ghttp.MethodPost, empty, false, []string{ghttp.MethodPost})
	PutAction           = NewAction(ghttp.MethodPut, empty, false, []string{ghttp.MethodPut})
	PatchAction         = NewAction(ghttp.MethodPatch, empty, false, []string{ghttp.MethodPatch})
	DeleteAction        = NewAction(ghttp.MethodDelete, empty, false, []string{ghttp.MethodDelete})
	RestActions         = []*Action{GetAction, PostAction, PutAction, PatchAction, DeleteAction}
	ListAction          = NewAction(list, empty, false, []string{ghttp.MethodGet})
	CreateAction        = NewAction(create, empty, false, []string{ghttp.MethodPost})
	RetrieveAction      = NewAction(retrieve, empty, true, []string{ghttp.MethodGet})
	UpdateAction        = NewAction(update, empty, true, []string{ghttp.MethodPut})
	PartialUpdateAction = NewAction(partialUpdate, empty, true, []string{ghttp.MethodPatch})
	DestroyAction       = NewAction(destroy, empty, true, []string{ghttp.MethodDelete})
	DefaultActionList   = []*Action{ListAction, RetrieveAction, CreateAction, UpdateAction, PartialUpdateAction, DestroyAction}
	DefaultActions      = DefaultActionList
)

type Action struct {
	HandlerName, UrlPath string
	Detail               bool
	Methods              []string
}

func NewAction(handlerName string, urlPath string, detail bool, methods []string) *Action {
	for _, method := range methods {
		for index, requestMethod := range ghttp.RequestMethods {
			if method == requestMethod {
				break
			} else if method != requestMethod && index == len(ghttp.RequestMethods)-1 {
				panic(fmt.Errorf("method `%s` is invalid, please choose from %v", method, requestMethod))
			}
		}
	}
	return &Action{HandlerName: handlerName, UrlPath: urlPath, Detail: detail, Methods: methods}
}

type Actions []*Action

type ActionGetter interface {
	GetActions() Actions
}
