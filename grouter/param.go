package grouter

var defaultUrlParam = "id"

type UrlParam interface {
	GetUrlParam() string
}

func SetDefaultUrlParam(urlParam string) {
	defaultUrlParam = urlParam
}
