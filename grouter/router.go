package grouter

import (
	"fmt"
	"gitee.com/Ivy1996-encode/ginrest/gaction"
	"gitee.com/Ivy1996-encode/ginrest/ghttp/handlers"
	"github.com/gin-gonic/gin"
	"reflect"
	"strings"
)

const (
	empty = ""
)

type Router struct {
	engine        *gin.Engine
	Group         *gin.RouterGroup
	middleware    gin.HandlersChain
	relativePath  string
	trailingSlash bool
}

func (r *Router) SetTrailingSlash(trailingSlash bool) {
	r.trailingSlash = trailingSlash
}

func (r *Router) SetRelativePath(relativePath string) {
	r.relativePath = relativePath
}

func (r *Router) register(path string, view interface{}, defaultActions gaction.Actions, middleware ...gin.HandlerFunc) {

	if r.Group == nil {
		r.Group = r.engine.Group(r.relativePath)
		r.Group.Use(handlers.RegisterHandlerErrorMiddlewares(r.middleware)...)
	}

	value := reflect.ValueOf(view)

	var actions gaction.Actions

	if actorGetter, ok := value.Interface().(gaction.ActionGetter); ok {
		actions = actorGetter.GetActions()
	} else {
		actions = defaultActions
	}

	for _, act := range actions {
		if handler := value.MethodByName(act.HandlerName); handler.IsValid() {
			if handlerFunc, ok := handler.Interface().(func(ctx *gin.Context)); ok {
				var relativePath string
				if act.Detail {
					var urlParam string
					if param, ok := value.Interface().(UrlParam); ok {
						if urlParam = param.GetUrlParam(); urlParam == empty {
							urlParam = defaultUrlParam
						}
					} else {
						urlParam = defaultUrlParam
					}
					if r.trailingSlash {
						relativePath = fmt.Sprintf("%s/:%s/", path, urlParam)
						if act.UrlPath != empty {
							relativePath = fmt.Sprintf("%s/%s/", relativePath, act.UrlPath)
						}
					} else {
						relativePath = fmt.Sprintf("%s/:%s", path, urlParam)
						if act.UrlPath != empty {
							relativePath = fmt.Sprintf("%s/%s", relativePath, act.UrlPath)
						}
					}
					for _, method := range act.Methods {
						middlewares := append(middleware, handlerFunc)
						r.Group.Handle(strings.ToTitle(method), relativePath, handlers.RegisterHandlerErrorMiddlewares(middlewares)...)
					}
				} else {
					if r.trailingSlash {
						relativePath = fmt.Sprintf("%s/", path)
						if act.UrlPath != empty {
							relativePath = fmt.Sprintf("%s/%s/", relativePath, act.UrlPath)
						}
					} else {
						relativePath = path
						if act.UrlPath != empty {
							relativePath = fmt.Sprintf("%s/%s", relativePath, act.UrlPath)
						}
					}
					for _, method := range act.Methods {
						middlewares := append(middleware, handlerFunc)
						r.Group.Handle(strings.ToTitle(method), relativePath, handlers.RegisterHandlerErrorMiddlewares(middlewares)...)
					}
				}
			}
		}
	}
}

func (r *Router) Register(path string, view interface{}, middleware ...gin.HandlerFunc) {
	r.register(path, view, gaction.RestActions, middleware...)
}

func NewRouter(engine *gin.Engine, middlewares ...gin.HandlerFunc) *Router {
	return &Router{engine: engine, middleware: middlewares, trailingSlash: true}
}

type DefaultRouter struct {
	Router
}

func (d *DefaultRouter) Register(path string, view interface{}, middleware ...gin.HandlerFunc) {
	d.register(path, view, gaction.DefaultActions, middleware...)
}

func NewDefaultRouter(engine *gin.Engine, middlewares ...gin.HandlerFunc) *DefaultRouter {
	return &DefaultRouter{Router: Router{engine: engine, middleware: middlewares, trailingSlash: true}}
}
