package exceptions

import (
	"gitee.com/Ivy1996-encode/ginrest/ghttp"
	"github.com/gin-gonic/gin"
)

var (
	PageNotFoundError     = pageNotFoundError{}
	MethodNotAllowedError = methodNotAllowedError{}
	Http404               = PageNotFoundError
	Http405               = MethodNotAllowedError
)

type RestError interface {
	HandlerError(context *gin.Context)
}

type pageNotFoundError struct{}

func (p pageNotFoundError) HandlerError(context *gin.Context) {
	ghttp.PageNotFoundHandler(context)
}

type methodNotAllowedError struct{}

func (m methodNotAllowedError) HandlerError(context *gin.Context) {
	ghttp.MethodNotAllowedHandler(context)
}
