# ginrest

> ginrest is simple, helpful , customizable, realize your idea with it!


## Limitation
```shell script
golang version >= 1.11
```

## Installation

####  `go get`

```shell
go get gitee.com/Ivy1996-encode/ginrest
```

#### `go mod`

```shell 
require gitee.com/Ivy1996-encode/ginrest 
```

## Example

`one struct with six routers!`

```go
package main

import "gitee.com/Ivy1996-encode/ginrest"

type User struct {
	ginrest.ModelViewSet
}

func main() {
	server := ginrest.Default()

	router := ginrest.DefaultRouter(server)

	router.Register("/user", new(User))

	server.Run()
}
```
```shell script
[GIN-debug] [WARNING] Creating an Engine instance with the Logger and Recovery middleware already attached.

[GIN-debug] [WARNING] Running in "debug" mode. Switch to "release" mode in production.
 - using env:   export GIN_MODE=release
 - using code:  gin.SetMode(gin.ReleaseMode)

[GIN-debug] GET    /user/                    --> gitee.com/Ivy1996-encode/ginrest/ghttp/handlers.RegisterHandlerErrorMiddleware.func1 (3 handlers)
[GIN-debug] GET    /user/:id/                --> gitee.com/Ivy1996-encode/ginrest/ghttp/handlers.RegisterHandlerErrorMiddleware.func1 (3 handlers)
[GIN-debug] POST   /user/                    --> gitee.com/Ivy1996-encode/ginrest/ghttp/handlers.RegisterHandlerErrorMiddleware.func1 (3 handlers)
[GIN-debug] PUT    /user/:id/                --> gitee.com/Ivy1996-encode/ginrest/ghttp/handlers.RegisterHandlerErrorMiddleware.func1 (3 handlers)
[GIN-debug] PATCH  /user/:id/                --> gitee.com/Ivy1996-encode/ginrest/ghttp/handlers.RegisterHandlerErrorMiddleware.func1 (3 handlers)
[GIN-debug] DELETE /user/:id/                --> gitee.com/Ivy1996-encode/ginrest/ghttp/handlers.RegisterHandlerErrorMiddleware.func1 (3 handlers)
[GIN-debug] Listening and serving HTTP on :8000

```


