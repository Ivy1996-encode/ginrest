package auth

import (
	"gitee.com/Ivy1996-encode/ginrest/ghttp"
	"github.com/gin-gonic/gin"
)

type UserGetter interface {
	GetCurrentUser(*gin.Context) User
}

type PermissionChecker interface {
	CheckPermission(*ghttp.Context)
	CheckObjectPermission(*ghttp.Context)
}

type Throttler interface {
	CheckThrottles(*ghttp.Context)
}

type Requester interface {
	UserGetter
	PermissionChecker
	Throttler
}

type AbstractRequester struct{}

func (a AbstractRequester) GetCurrentUser(context *gin.Context) User { return nil }

func (a AbstractRequester) CheckPermission(context *ghttp.Context) {}

func (a AbstractRequester) CheckObjectPermission(context *ghttp.Context) {}

func (a AbstractRequester) CheckThrottles(context *ghttp.Context) {}
