package auth

type User interface {
	IsAuthenticated() bool
}

type AnonymousUser struct{}

func (a AnonymousUser) IsAuthenticated() bool {
	return false
}
