package ghttp

import (
	"gitee.com/Ivy1996-encode/ginrest/contrib/auth"
	"github.com/gin-gonic/gin"
)

type HandlerFunc func(*Context)

type Context struct {
	*gin.Context
	User auth.User
}

func NewContext(context *gin.Context, user auth.User) *Context {
	return &Context{Context: context, User: user}
}
