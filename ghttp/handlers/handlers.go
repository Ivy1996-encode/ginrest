package handlers

import (
	"gitee.com/Ivy1996-encode/ginrest/contrib/auth"
	"gitee.com/Ivy1996-encode/ginrest/exceptions"
	"gitee.com/Ivy1996-encode/ginrest/ghttp"
	"github.com/gin-gonic/gin"
)

func HandlerErrorMiddleware() func(context *gin.Context) {
	return func(context *gin.Context) {
		if err := recover(); err != nil {
			if gError, ok := err.(exceptions.RestError); ok {
				gError.HandlerError(context)
				context.Abort()
			} else {
				panic(err)
			}
		}
	}
}

func RegisterHandlerErrorMiddleware(handlerFunc gin.HandlerFunc) gin.HandlerFunc {
	return func(context *gin.Context) {
		defer HandlerErrorMiddleware()(context)
		handlerFunc(context)
	}
}

func RegisterHandlerErrorMiddlewares(middlewares gin.HandlersChain) gin.HandlersChain {
	handledMiddleware := make(gin.HandlersChain, 0)
	for _, middleware := range middlewares {
		handledMiddleware = append(handledMiddleware, RegisterHandlerErrorMiddleware(middleware))
	}
	return handledMiddleware
}

func AsHandlerFunc(handlerFunc ghttp.HandlerFunc, requester auth.Requester) gin.HandlerFunc {
	return func(context *gin.Context) {
		var user auth.User
		if user := requester.GetCurrentUser(context); user == nil {
			user = &auth.AnonymousUser{}
		}
		newContext := ghttp.NewContext(context, user)
		requester.CheckObjectPermission(newContext)
		requester.CheckThrottles(newContext)
		handlerFunc(newContext)
	}
}
