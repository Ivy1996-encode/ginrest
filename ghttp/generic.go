package ghttp

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

var (
	pageNotFoundResponse     interface{}
	methodNotAllowedResponse interface{}
	forbiddenResponse        interface{}
)

func PageNotFoundHandler(context *gin.Context) {
	context.JSON(http.StatusNotFound, pageNotFoundResponse)
}

func MethodNotAllowedHandler(context *gin.Context) {
	context.JSON(http.StatusMethodNotAllowed, methodNotAllowedResponse)
}

func ForbiddenHandler(context *gin.Context) {
	context.JSON(http.StatusForbidden, forbiddenResponse)
}

func SetPageNotFoundResponse(response interface{}) {
	pageNotFoundResponse = response
}

func SetMethodNotAllowedResponse(response interface{}) {
	methodNotAllowedResponse = response
}

func SetForbiddenResponse(response interface{}) {
	forbiddenResponse = response
}

func init() {
	pageNotFoundResponse = gin.H{"detail": "Not Found"}
	methodNotAllowedResponse = gin.H{"detail": "Method Not Allowed"}
	forbiddenResponse = gin.H{"detail": "Forbidden request"}
}
