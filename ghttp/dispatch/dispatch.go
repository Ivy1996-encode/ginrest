package dispatch

import (
	"gitee.com/Ivy1996-encode/ginrest/exceptions"
	"gitee.com/Ivy1996-encode/ginrest/ghttp"
	"gitee.com/Ivy1996-encode/ginrest/ghttp/handlers"
	"github.com/gin-gonic/gin"
	"reflect"
	"strings"
)

type RequestDispatcher interface {
	DispatchRequest(context *gin.Context)
}

type MapRequestDispatcher map[string]gin.HandlerFunc

func (r MapRequestDispatcher) DispatchRequest(context *gin.Context) {

	defer handlers.HandlerErrorMiddleware()(context)

	requestMethod := strings.Title(strings.ToLower(context.Request.Method))

	if handler := r[requestMethod]; handler != nil {
		handler(context)
	} else {
		panic(exceptions.MethodNotAllowedError)
	}
}

func MakeStructAsHandlerFunc(view interface{}) gin.HandlerFunc {

	value := reflect.ValueOf(view)

	requestDispatcher := make(MapRequestDispatcher)

	for _, requestMethod := range ghttp.RequestMethods {

		if handler := value.MethodByName(requestMethod); handler.IsValid() {

			if methodHandler, ok := handler.Interface().(func(ctx *gin.Context)); ok {
				requestDispatcher[requestMethod] = handlers.RegisterHandlerErrorMiddleware(methodHandler)
			}
		}
	}

	return func(context *gin.Context) {
		requestDispatcher.DispatchRequest(context)
	}
}
