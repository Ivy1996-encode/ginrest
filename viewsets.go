package ginrest

import (
	"gitee.com/Ivy1996-encode/ginrest/exceptions"
	"github.com/gin-gonic/gin"
)

type ListView struct{}

func (*ListView) List(context *gin.Context) {
	panic(exceptions.MethodNotAllowedError)
}

type RetrieveView struct{}

func (*ListView) Retrieve(context *gin.Context) {
	panic(exceptions.MethodNotAllowedError)
}

type CreateView struct{}

func (*CreateView) Create(context *gin.Context) {
	panic(exceptions.MethodNotAllowedError)
}

type UpdateView struct{}

func (*UpdateView) Update(context *gin.Context) {
	panic(exceptions.MethodNotAllowedError)
}

type PartialUpdateView struct{}

func (*PartialUpdateView) PartialUpdate(context *gin.Context) {
	panic(exceptions.MethodNotAllowedError)
}

type DestroyView struct{}

func (m *DestroyView) Destroy(context *gin.Context) {
	panic(exceptions.MethodNotAllowedError)
}

type ReadOnlyViewSet struct {
	ListView
	RetrieveView
}

type UpdateViewSet struct {
	UpdateView
	PartialUpdateView
}

type ModelViewSet struct {
	ReadOnlyViewSet
	CreateView
	UpdateViewSet
	DestroyView
}
